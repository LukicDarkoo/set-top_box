#include "tdp_api.h"
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include "HPlayer.h"
#include "Remote.h"
#include "Graphics.h"
#include "Config.h"
#include <stdbool.h>
#include "utils/Timer.h"

static bool shouldExit = false;
static int16_t requiredChannel = 0;
static int16_t currentChannel = 0;
static uint8_t servicesCount;

static uint8_t currentVolume = 3;
static uint64_t eventStartTime;
static char eventText[50];
static bool volumeMuted = false;
static bool requiredChannelAvailable = true;
static Timer channelTimer;
static const uint8_t WAIT_AFTER_KEY_PRESS = 20;
static const uint16_t SHOW_DIALOG_MIN_TIME = 3000;

/**
 * @brief Callback function which will be called after user stops stressing
 */
static void makeRequiredChannelAvailable() {
	printf("makeRequiredChannelAvailable\n");
	requiredChannelAvailable = true;
}

/**
 * @brief Callback function called on key press
 * @param [in] keycode Key code of pressed key
 * @return Error code
 */
static int8_t onKeyPressed(int16_t keycode) {
	switch(keycode) {
	case REMOTE_CHANNEL_UP:
		requiredChannelAvailable = false;
		Timer_SetDelay(&channelTimer, WAIT_AFTER_KEY_PRESS);

		if (requiredChannel >= servicesCount) {
			requiredChannel = 1;
		} else {
			requiredChannel++;
		}

		Graphics_ShowChannelInfoDialog(SHOW_DIALOG_MIN_TIME);
		Graphics_SetChannelNumber(requiredChannel);
		Graphics_SetTeletext(false);
		Graphics_SetPIDs(0, 0);
		break;

	case REMOTE_CHANNEL_DOWN:
		requiredChannelAvailable = false;
		Timer_SetDelay(&channelTimer, WAIT_AFTER_KEY_PRESS);

		if (requiredChannel == 1) {
			requiredChannel = servicesCount;
		} else {
			requiredChannel--;
		}

		Graphics_ShowChannelInfoDialog(SHOW_DIALOG_MIN_TIME);
		Graphics_SetChannelNumber(requiredChannel);
		Graphics_SetTeletext(false);
		Graphics_SetPIDs(0, 0);
		break;

	case REMOTE_CHANNEL_EXIT:
		shouldExit = true;
		break;

	case REMOTE_CHANNEL_1 ... REMOTE_CHANNEL_9:
		if (keycode - 1 <= servicesCount) {
			requiredChannel = keycode - 1;
			requiredChannelAvailable = false;
			Timer_SetDelay(&channelTimer, WAIT_AFTER_KEY_PRESS);
			Graphics_SetTeletext(false);
			Graphics_SetPIDs(0, 0);
			Graphics_SetChannelNumber(requiredChannel);
			Graphics_ShowChannelInfoDialog(SHOW_DIALOG_MIN_TIME);
		} else {
			Graphics_ShowNoChannel(true);
		}
		break;

	case REMOTE_CHANNEL_INFO:
		Graphics_ShowChannelInfoDialog(SHOW_DIALOG_MIN_TIME);
		Graphics_ShowShortEventDialog(SHOW_DIALOG_MIN_TIME);
		break;

	case REMOTE_VOLUME_UP:
		volumeMuted = false;
		if (currentVolume < 10) {
			currentVolume++;
		}
		Graphics_SetVolume(currentVolume);
		HPlayer_SetVolume(currentVolume);
		Graphics_ShowVolume(SHOW_DIALOG_MIN_TIME);
		break;

	case REMOTE_VOLUME_DOWN:
		volumeMuted = false;
		if (currentVolume > 0) {
			currentVolume--;
			Graphics_SetVolume(currentVolume);
			HPlayer_SetVolume(currentVolume);
		}
		Graphics_ShowVolume(SHOW_DIALOG_MIN_TIME);
		break;

	case REMOTE_VOLUME_MUTE:
		if (volumeMuted == false) {
			volumeMuted = true;
			Graphics_SetVolume(0);
			HPlayer_SetVolume(0);

		} else {
			volumeMuted = false;
			Graphics_SetVolume(currentVolume);
			HPlayer_SetVolume(currentVolume);
		}

		Graphics_ShowVolume(SHOW_DIALOG_MIN_TIME);
		break;

	default:
		printf("Undefined channel! Key pressed = %d\n", keycode);
	}

	return 0;
}

/**
 * @brief Callback function called on channel changed
 * @param [in] channel Active channel
 * @return Error code
 */
static int8_t onChannelChanged(int32_t channel) {
	uint16_t currentVideoPID;
	uint16_t currentAudioPID;
	HPlayer_GetAudioPID(&currentAudioPID);
	HPlayer_GetVideoPID(&currentVideoPID);

	Graphics_SetChannelNumber(channel);
	Graphics_SetPIDs(currentAudioPID, currentVideoPID);
	Graphics_ShowChannelInfoDialog(SHOW_DIALOG_MIN_TIME);

	Graphics_SetRadioScreen(HPlayer_HasVideo() == false);
	Graphics_ShowNoChannel(false);
	Graphics_SetTeletext(HPlayer_HasTeletext());
	Graphics_SetShortEvent("", 0);

	printf("onChannelChanged\n");

	return 0;
}

/**
 * Callback function call when additional information about channel is arrived
 * @return Error code
 */
static int8_t onInfoArrived() {
	int8_t statusEventText;
	int8_t statusStartTime;

	statusEventText = HPlayer_GetEventText(eventText);
	statusStartTime = HPlayer_GetEventStartTime(&eventStartTime);

	if (statusEventText == HPLAYER_SUCCESS && statusStartTime == HPLAYER_SUCCESS) {
		Graphics_SetShortEvent(eventText, eventStartTime);
		Graphics_ShowShortEventDialog(SHOW_DIALOG_MIN_TIME);
	}

	return 0;
}

int main(int32_t argc, char** argv) {
	Timer_Init(&channelTimer, makeRequiredChannelAvailable);
	Config_Init("config.ini");
	Graphics_Init(argc, argv);
	Remote_Init(onKeyPressed);

	// Init hplayer
	HPlayer_Init();
	HPlayer_SetChangeChannelCallback(onChannelChanged);
	HPlayer_SetEventInfoArrivedCallback(onInfoArrived);

	HPlayer_SetVolume(currentVolume);


	HPlayer_PreSetChannel(
		Config_GetInt32("video_pid"),
		Config_GetInt32("audio_pid"),
		Config_GetInt32("dtvb_video_type"),
		Config_GetInt32("dtvb_audio_type")
	);

	requiredChannel = Config_GetInt32("program");
	HPlayer_Scan();
	HPlayer_GetServiceInfoCount(&servicesCount);

	// Pause
    while (shouldExit == false) {
    	if (requiredChannelAvailable == true && currentChannel != requiredChannel) {
    		currentChannel = requiredChannel;
    		HPlayer_SetChanel(currentChannel);
    	}
    }


	HPlayer_Deinit(); 
	Remote_Deinit();
	Graphics_Deinit();
	Config_Deinit();
	Timer_Deinit(&channelTimer);

    return 0;
}

