#ifndef _REMOTE_H_
#define _REMOTE_H_

#include <stdio.h>
#include <linux/input.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>
#include <errno.h>

#define NUM_EVENTS 1

#define REMOTE_EV_VALUE_RELEASE    0
#define REMOTE_EV_VALUE_KEYPRESS   1
#define REMOTE_EV_VALUE_AUTOREPEAT 2

#define REMOTE_ERROR 1
#define REMOTE_SUCCESS 0

#define REMOTE_CHANNEL_UP 62
#define REMOTE_CHANNEL_DOWN 61
#define REMOTE_CHANNEL_EXIT 102
#define REMOTE_CHANNEL_INFO 358
#define REMOTE_CHANNEL_1 2
#define REMOTE_CHANNEL_2 3
#define REMOTE_CHANNEL_3 4
#define REMOTE_CHANNEL_4 5
#define REMOTE_CHANNEL_5 6
#define REMOTE_CHANNEL_6 7
#define REMOTE_CHANNEL_7 8
#define REMOTE_CHANNEL_8 9
#define REMOTE_CHANNEL_9 10
#define REMOTE_VOLUME_UP 63
#define REMOTE_VOLUME_DOWN 64
#define REMOTE_VOLUME_MUTE 60

/**
 * @brief Initialize remote
 * @param [in] callback Pointer to callback function which will be called after key is pressed
 * @return Error code
 */
int8_t Remote_Init(int8_t (*callback)(int16_t keycode));

/**
 * @brief Deinitialize remote
 * @return Error code
 */
int8_t Remote_Deinit();

#endif
