#ifndef _HPLAYER_H_
#define _HPLAYER_H_

/**
 * @brief Module enables easy control of demux and player
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <errno.h>
#include <stdbool.h>
#include <time.h>
#include "tdp_api.h"
#include "parsers/PAT.h"
#include "parsers/PMT.h"
#include "parsers/EIT.h"

#define HPLAYER_SUCCESS 0
#define HPLAYER_ERROR -1
#define HPLAYER_EIT_NOT_INITIALIZED -2

/**
 * @brief Initialize stream
 * @return Error code
 */
int8_t HPlayer_Init();

/**
 * @brief Scan for available channels (PAT table)
 * @return Error code
 */
int8_t HPlayer_Scan();

/**
 * @brief Deinitialize stream
 * @return Error code
 */
int8_t HPlayer_Deinit();

/**
 * @brief Callback function will be called after channel is changed
 * @param [in] callback Pointer to callback function
 * @return Error code
 */
int8_t HPlayer_SetChangeChannelCallback(int8_t (*callback)(int32_t channel));

/**
 * @brief Callback function is called after additional information about channel are received
 * @param [in] callback Pointer to callback function
 * @return Error code
 */
int8_t HPlayer_SetEventInfoArrivedCallback(int8_t (*callback)());

/**
 * @brief Change channel
 * @param [in] chanel Required index of channel
 * @return Error code
 */
int8_t HPlayer_SetChanel(int32_t chanel);

/**
 * @brief Get name of current short event
 * @param [out] text Event text
 * @return Error code
 */
int8_t HPlayer_GetEventText(char* text);

/**
 * @brief Get number of scanned channels
 * @param [out] servicesCount Number of scanned channels
 * @return Error code
 */
int8_t HPlayer_GetServiceInfoCount(uint8_t* servicesCount);

/**
 * @brief Get start time of current short event
 * @param [out] time Short event start time
 * @return Error code
 */
int8_t HPlayer_GetEventStartTime(uint64_t* time);

/**
 * @brief Get video PID of current channel
 * @param [out] videoPID Current video PID
 * @return Error code
 */
int8_t HPlayer_GetVideoPID(uint16_t* videoPID);

/**
 * @brief Get audio PID of current channel
 * @param [out] videoPID Current audio PID
 * @return Error code
 */
int8_t HPlayer_GetAudioPID(uint16_t* audioPID);

/**
 * @brief Get information about teletext existence for current channel
 * @return True if teletext exists
 */
bool HPlayer_HasTeletext();

/**
 * @brief Get information about video existence for current channel
 * @return True if video exists
 */
bool HPlayer_HasVideo();

/**
 * @brief Set volume
 * @param [in] volume Volume in range [0, 10]
 * @return Error code
 */
int8_t HPlayer_SetVolume(uint8_t volume);

/**
 * @brief Fast way to set channel without parsing any tables
 * @param [in] videoPID Video PID
 * @param [in] audioPID Audio PID
 * @param [in] videoType Video type (DTV)
 * @param [in] audioType Audio type (DTV)
 * @return Error code
 */
int8_t HPlayer_PreSetChannel(uint16_t videoPID, uint16_t audioPID, uint8_t videoType, uint8_t audioType);

#endif
