#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <stdio.h>
#include <directfb.h>
#include <inttypes.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>
#include "utils/Timer.h"

#define DFBCHECK(x...)                                      \
{                                                           \
DFBResult err = x;                                          \
                                                            \
if (err != DFB_OK)                                          \
  {                                                         \
    fprintf( stderr, "%s <%d>:\n\t", __FILE__, __LINE__ );  \
    DirectFBErrorFatal( #x, err );                          \
  }                                                         \
}

#define GRAPHICS_SUCCESS 0
#define GRAPHICS_ERROR 1
#define GRAPHICS_FPS 60

#define GRAPHICS_REFRESH_PERIOD (1000 / GRAPHICS_FPS)

/**
 * @brief Graphics parameters for channel info dialog
 */
typedef struct _ChannelInfoDialog {
	uint32_t number;
	bool show;
	bool teletextAvailable;
	uint32_t audioPID;
	uint32_t videoPID;
} ChannelInfoDialog;

/**
 * @brief Graphics parameters for short event dialog
 */
typedef struct _ShortEvent {
	bool show;
	char text[200];
	char startTime[80];
} ShortEvent;

/**
 * @brief Graphics parameters for volume graphics
 */
typedef struct _Volume {
	bool show;
	uint32_t imageIndex;
} Volume;

/**
 * @brief Configuration values for graphics
 */
typedef struct _VisualState {
	ChannelInfoDialog channelInfoDialog;
	ShortEvent shortEvent;
	Volume volume;
	bool radioScreen;
	bool noChannel;
} VisualState;

/**
 * @brief Initialize graphics modules
 * @param [in] argc Number of program arguments
 * @param [in] argv Array of program arguments values
 * @return Error code (GRAPHICS_SUCCESS|GRAPHICS_ERROR)
 */
int8_t Graphics_Init(int32_t argc, char** argv);

/**
 * @brief Deinitialize graphics module
 * @return Error code
 */
int8_t Graphics_Deinit();

/**
 * @brief Set values for channel number
 * @param [in] channel Channel number
 * @return Error code
 */
int8_t Graphics_SetChannelNumber(uint8_t channel);

/**
 * @brief Set parameters for short event dialog
 * @param [in] text Text which should be displayed in short event dialog
 * @param [in] startTime Start of event
 * @return Error code
 */
int8_t Graphics_SetShortEvent(char* text, uint64_t startTime);

/**
 * @brief Show channel info dialog
 * @param [in] msDuration Visibility duration of channel info dialog in ms
 * @return Error code
 */
int8_t Graphics_ShowChannelInfoDialog(uint32_t msDuration);

/**
 * @brief Show short event dialog
 * @param [in] msDuration Visibility duration of short event dialog in ms
 * @return Error code
 */
int8_t Graphics_ShowShortEventDialog(uint32_t msDuration);

/**
 * @brief Show or hide radio screen
 * @param [in] show True to show or False to hide
 * @return Error code
 */
int8_t Graphics_SetRadioScreen(bool show);

/**
 * @brief Set audio and video PID value for channel info dialog
 * @param [in] audioPID Value for audio PID
 * @param [in] videoPID Value for video PID
 * @return Error code
 */
int8_t Graphics_SetPIDs(uint16_t audioPID, uint16_t videoPID);

/**
 * @brief Set does teletext exist for current channel
 * @param [in] exists True of exists or False if not exists
 * @return Error code
 */
int8_t Graphics_SetTeletext(bool exists);

/**
 * @brief Show or hide no channel screen
 * @param [in] show True to show or False to hide
 * @return Error code
 */
int8_t Graphics_ShowNoChannel(bool show);

/**
 * @brief Set volume intensity to be shown
 * @param [in] volume Volume in range [0 - 10]
 * @return Error code
 */
int8_t Graphics_SetVolume(uint8_t volume);

/**
 * @brief Show volume dialog
 * @param [in] msDuration Visibility duration of volume image in ms
 * @return Error code
 */
int8_t Graphics_ShowVolume(uint32_t msDuration);

#endif
