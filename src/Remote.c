#include "Remote.h"

static int8_t listen;
static int8_t (*onKeyPressedCallback)(int16_t keycode);
static int32_t inputFileDesc;
static void* listenForKeys();
static int32_t getKeys(int32_t count, uint8_t* buf, int32_t* eventsRead);

int8_t Remote_Init(int8_t (*callback)(int16_t keycode)) {
	pthread_t remote;
	listen = 1;

	// Save pointer on callback function
	onKeyPressedCallback = callback;

	// Create thread
	if(pthread_create(&remote, NULL, &listenForKeys, NULL)) {
		printf("Error creating input_event_task!\n");
		return REMOTE_ERROR;
	}

	return REMOTE_SUCCESS;
}

int8_t Remote_Deinit() {
	listen = 0;
	return REMOTE_SUCCESS;
}

int32_t getKeys(int32_t count, uint8_t* buf, int32_t* eventsRead) {
    int32_t ret = 0;

    // Read next event & put it in buffer
    ret = read(inputFileDesc, buf, (size_t)(count * (int)sizeof(struct input_event)));
    if(ret <= 0)
    {
        printf("Error code %d", ret);
        return REMOTE_ERROR;
    }

    // Calculate number of read events
    *eventsRead = ret / (int)sizeof(struct input_event);

    return REMOTE_SUCCESS;
}

void* listenForKeys() {
    char deviceName[20];
    struct input_event eventBuf;
    uint32_t eventCnt;
    int32_t counter = 0;
    const char* dev = "/dev/input/event0";

    inputFileDesc = open(dev, O_RDWR);
    if(inputFileDesc == -1)
    {
        printf("Error while opening device (%s) !", strerror(errno));
		return (void*)REMOTE_ERROR;
    }

    // Get the name of input device
    ioctl(inputFileDesc, EVIOCGNAME(sizeof(deviceName)), deviceName);
	printf("RC device opened successfully [%s]\n", deviceName);

    while(listen == 1) {

        // Read next input event
        if(getKeys(NUM_EVENTS, (uint8_t*)&eventBuf, &eventCnt))
        {
			printf("Error while reading input events !");
			return (void*)REMOTE_ERROR;
		}

		// Filter input events */
        if(eventBuf.type == EV_KEY &&
          (eventBuf.value == REMOTE_EV_VALUE_KEYPRESS || eventBuf.value == REMOTE_EV_VALUE_AUTOREPEAT))
        {

        	onKeyPressedCallback(eventBuf.code);
		}
    }
	return (void*)REMOTE_SUCCESS;
}
