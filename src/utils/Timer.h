#ifndef _TIMER_H_
#define _TIMER_H_

/**
 * @brief Module enables easy timer management
 */

#include <signal.h>
#include <time.h>
#include <string.h>
#include <inttypes.h>
#include <stdio.h>

#define TIMER_ERROR -1
#define TIMER_ERROR_CREATE -2
#define TIMER_ERROR_SET_TIMER -3
#define TIMER_SUCCESS 0

/**
 * @brief Describes timer's internal parameters. Should be initialized for every
 * timer.
 */
typedef struct _Timer {
	struct itimerspec timerSpec;
	struct itimerspec timerSpecOld;
	timer_t timerId;
	struct sigevent signalEvent;
	void (*callback)();
} Timer;

/**
 * @brief Timer initialization
 * @param [in] timer Pointer to Timer structure
 * @param [in] callback Function to be called on event
 * @return Error code
 */
int8_t Timer_Init(Timer* timer, void (*callback)());

/**
 * @brief Set time to wait before call a callback function
 * @param [in] timer Pointer to Timer structure
 * @param [in] ms Time to wait before call a callback
 * @return Error code
 */
int8_t Timer_SetDelay(Timer* timer, uint16_t ms);

/**
 * @brief Timer deinitialization
 * @param [in] timer Pointer to Timer structure
 * @return Error code
 */
int8_t Timer_Deinit(Timer* timer);

#endif
