#include "Timer.h"

/**
 * @brief Internal callback function for all timers
 * @param [in] signalArg Timer parameters
 */
static void timerCallback(union sigval signalArg);

int8_t Timer_Init(Timer* timer, void (*callback)()) {
	timer->signalEvent.sigev_notify = SIGEV_THREAD;
	timer->signalEvent.sigev_notify_function = timerCallback;
	timer->signalEvent.sigev_value.sival_ptr = timer;
	timer->signalEvent.sigev_notify_attributes = NULL;
	timer->callback = callback;

	if(timer_create(CLOCK_REALTIME, &(timer->signalEvent), &(timer->timerId)) != 0) {
		return TIMER_ERROR_CREATE;
	}

	return TIMER_SUCCESS;
}


int8_t Timer_SetDelay(Timer* timer, uint16_t ms) {
	memset(&(timer->timerSpec), 0, sizeof(timer->timerSpec));
    
    (timer->timerSpec).it_value.tv_sec = ms / 1000;
    (timer->timerSpec).it_value.tv_nsec = (ms % 1000) * (ms * 1000 * 1000);
    
    if (timer_settime(timer->timerId, 0, &(timer->timerSpec), &(timer->timerSpecOld))) {
		return TIMER_ERROR_SET_TIMER;
    }

	return TIMER_SUCCESS;
}

int8_t Timer_Deinit(Timer* timer) {
	timer_delete(timer->timerId);

	return TIMER_SUCCESS;
}

static void timerCallback(union sigval signalArg) {
	Timer* timer = ((Timer *)signalArg.sival_ptr);

	memset(&(timer->timerSpec), 0, sizeof(timer->timerSpec));
	
	if (timer_settime(timer->timerId, 0, &(timer->timerSpec), &(timer->timerSpecOld))) {
		printf("ERROR: Timer callback!\n");
	}

	timer->callback();
}
