#include "Config.h"

static bool loaded = false;
static uint8_t** keys;
static uint8_t** values;
static numberOfParams;

/**
 * @brief Find index by key
 * @param [in] key Key for required index
 * @return Index in array
 */
static int16_t findKeyIndex(const uint8_t* const key);

int8_t Config_Init(const uint8_t* const filename) {
	int i;
	char line[CONFIG_MAX_LINE_LENGTH];
	FILE* file;

	keys = (uint8_t**)malloc(sizeof(uint8_t*));
	values = (uint8_t**)malloc(sizeof(uint8_t*));

	if (loaded == true) {
		return CONFIG_ALREADY_LOADED;
	}

	if ((file = fopen((char*)filename, "r")) == NULL) {
		return CONFIG_NO_FILE;
	}

	i = 0;
	while (fgets(line, sizeof(line), file)) {
		char* delimiterIndex = strchr(line, '=');
		if (delimiterIndex == NULL) {
			continue;
		}

		// Alocate memory
		keys = realloc(keys, (i + 1) * sizeof(uint8_t*));
		values = realloc(values, (i + 1) * sizeof(uint8_t*));
		keys[i] = (uint8_t*)malloc(delimiterIndex - line + 1);
		values[i] = (uint8_t*)malloc(strlen(delimiterIndex) + 1);

		// Put to proper arrays
		strncpy(keys[i], line, delimiterIndex - line);
		strncpy(values[i], delimiterIndex + 1, strlen(delimiterIndex) - 2); // -2 to remove \0\n

		i++;
	}

	numberOfParams = i;
	loaded = true;
	return CONFIG_SUCCESS;
}

int32_t Config_GetInt32(const uint8_t* const key) {
	int16_t index = findKeyIndex(key);

	assert(index >= 0);

	return atoi(values[index]);
}

int8_t* Config_GetString(const uint8_t* const key) {
	int16_t index = findKeyIndex(key);

	assert(index >= 0);

	return values[index];
}

int8_t Config_Deinit() {
	int16_t i;

	if (loaded == false) {
		return CONFIG_NOT_LOADED;
	}

	for (i = 0; i < numberOfParams; i++) {
		free(keys[i]);
		free(values[i]);
	}

	free(values);
	free(keys);

	loaded = false;

	return CONFIG_SUCCESS;
}

int8_t Config_Dump() {
	int16_t i;

	if (loaded == false) {
		return CONFIG_NOT_LOADED;
	}

	printf("Dump Config\n");
	printf("===========\n");
	for (i = 0; i < numberOfParams; i++) {
		printf("  %s=%s\n", keys[i], values[i]);
	}

	return CONFIG_SUCCESS;
}

int16_t findKeyIndex(const uint8_t* const key) {
	int16_t i;

	if (loaded == false) {
		return CONFIG_NOT_LOADED;
	}

	for (i = 0; i < numberOfParams; i++) {
		if (strcmp(key, keys[i]) == 0) {
			return i;
		}
	}

	return CONFIG_NO_KEY;
}
