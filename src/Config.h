#ifndef _CONFIG_H_
#define _CONFIG_H_

/**
 * @brief Module enables easy access to configuration file
 * @author	Darko Lukic <lukicdarkoo@gmail.com>
 *
 */

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#define CONFIG_MAX_LINE_LENGTH 255
#define CONFIG_MAX_KEY_LENGTH 15
#define CONFIG_MAX_VALUE_LENGTH 20

#define CONFIG_ERROR -1
#define CONFIG_NOT_LOADED -2
#define CONFIG_NO_FILE -3
#define CONFIG_ALREADY_LOADED -4
#define CONFIG_NO_KEY -5
#define CONFIG_SUCCESS 0


/**
 * @brief Initialize configuration
 * @param [in] filename Filename of configuration file
 * @return Error code
 */
int8_t Config_Init(const uint8_t* const filename);


/**
 * @brief Get integer parameter
 * @param [in] key Key for required value
 * @return Value
 */
int32_t Config_GetInt32(const uint8_t* const key);


/**
 * @brief Get string parameter
 * @param [in] key Key for required value
 * @return Value
 */
int8_t* Config_GetString(const uint8_t* const key);


/**
 * @brief Config deinitialization
 * @return Error code
 */
int8_t Config_Deinit();


/**
 * @brief Print all key value pairs
 * @return Error code
 */
int8_t Config_Dump();

#endif
