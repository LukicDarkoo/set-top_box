#include "EIT.h"



int8_t EIT_Parse(uint8_t* buffer, EITTable* eitTable) {
	int16_t sectionLengthCounter;
	int16_t eventDescriptorsStartOffset;

	eitTable->serviceId = BitsBufferSlice(buffer, 24, 16);
	eitTable->sectionLength = BitsBufferSlice(buffer, 12, 12);
	eitTable->eventCount = 0;

	// Offset to first loop
	sectionLengthCounter = 11 + 3;

	while (sectionLengthCounter < eitTable->sectionLength - 4 + 3) {
		eitTable->eitEvent[eitTable->eventCount].eventId = BitsBufferSlice(buffer, sectionLengthCounter * 8, 12);
		eitTable->eitEvent[eitTable->eventCount].startTime = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 16, 40);
		eitTable->eitEvent[eitTable->eventCount].startTime |= ((uint64_t)buffer[sectionLengthCounter + 2] << 32);
		eitTable->eitEvent[eitTable->eventCount].duration = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 56, 24);
		eitTable->eitEvent[eitTable->eventCount].runningStatus = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 80, 3);
		eitTable->eitEvent[eitTable->eventCount].descriptorsLoopLength = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 84, 12);
		sectionLengthCounter += 12;
		eventDescriptorsStartOffset = sectionLengthCounter;

		// Descriptors
		while (sectionLengthCounter < eventDescriptorsStartOffset + eitTable->eitEvent[eitTable->eventCount].descriptorsLoopLength) {
			EITShortEventDescriptor eventDescriptor;
			eventDescriptor.descriptionTag = buffer[sectionLengthCounter];
			eventDescriptor.descriptionLength = buffer[sectionLengthCounter + 1];

			if (eventDescriptor.descriptionTag == 0x4D) {
				// Save event name
				eventDescriptor.eventNameLength = buffer[sectionLengthCounter + 5];
				memcpy(eventDescriptor.eventName, (buffer + sectionLengthCounter + 7), eventDescriptor.eventNameLength - 1);
				eventDescriptor.eventName[eventDescriptor.eventNameLength - 1] = '\0';

				eitTable->eitEvent[eitTable->eventCount].eitShortEventDescriptor = eventDescriptor;
			}

			sectionLengthCounter += 2 + eventDescriptor.descriptionLength;
		}
	    eitTable->eventCount++;
	}

	return EIT_SUCCESS;
}



int8_t EIT_Dump(EITTable* eitTable) {
	uint8_t i;

	printf("\n\nEIT Table\n");
	printf("serviceId = %d\n", eitTable->serviceId);
	printf("sectionLength = %d\n", eitTable->sectionLength);
	printf("eventCount = %d\n", eitTable->eventCount);

	for (i = 0; i < eitTable->eventCount; i++) {
		printf("  eventId = %d\n", eitTable->eitEvent[i].eventId);
		printf("  startTime = 0x%" PRIx64 "\n", eitTable->eitEvent[i].startTime);
		printf("  duration = 0x%x\n", eitTable->eitEvent[i].duration);
		printf("  runningStatus = %d\n", eitTable->eitEvent[i].runningStatus);
		printf("  descriptorsLoopLength = %d\n", eitTable->eitEvent[i].descriptorsLoopLength);

		printf("  eitShortEventDescriptor.eventName = %s\n", eitTable->eitEvent[i].eitShortEventDescriptor.eventName);

		printf("------------------\n");
	}

	return EIT_SUCCESS;
}

