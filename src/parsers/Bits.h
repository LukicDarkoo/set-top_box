#ifndef _BITS_H_

/**
 * @brief	Module enables easy byte manipulation. Function name pattern: Bits[Byte|Buffer][Operation]
 * @author	Darko Lukic <lukicdarkoo@gmail.com>
 */

#include <stdio.h>
#include <inttypes.h>


/**
 * @brief	Print bits
 * @param	[in] value Value to print
 * @param	[in] numberOfBytes Size of value in bytes
 */
void BitsByteDump(uint64_t value, uint8_t numberOfBytes);


/**
 * @brief  Slice bits from byte
 *
 * @param  [in] byte  Byte to be sliced
 * @param  [in] start Start bit
 * @param  [in] length Number of bits to be sliced
 * @return Error code
 */
uint8_t BitsByteSlice(uint8_t byte, uint8_t start, uint8_t length);


/**
 * @brief	Slice bits from buffer
 * @param 	[in] buffer Buffer
 * @param	[in] start Index of start bit
 * @param	[in] length Number of bits to slice
 *
 * @return	Sliced bits
 */
uint64_t BitsBufferSlice(uint8_t* buffer, uint16_t start, uint8_t length);


uint8_t BitsByteSet(uint8_t value, uint8_t position);
uint8_t BitsByteClear(uint8_t value, uint8_t position);
uint8_t BitsByteGet(uint8_t value, uint8_t position);

#endif // _BITS_H_
