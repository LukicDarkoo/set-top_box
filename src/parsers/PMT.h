#ifndef _PMT_H_
#define _PMT_H_

#include "Bits.h"
#include <inttypes.h>
#include <stdbool.h>

#define PMT_MAX_ITEMS 80	/* Max number of items in PMT table */
#define PMT_VIDEO 0x02
#define PMT_AUDIO 0x03

#define PMT_ERROR 1
#define PMT_SUCCESS 0

/**
 * @brief Parameters for each stream
 */
typedef struct _PMTItem {
	uint16_t streamType;
	uint16_t elementaryPID;
	uint16_t ESInfoLength;
} PMTItem;

/**
 * @brief PMT table
 */
typedef struct _PMTTable {
	uint16_t sectionLength;
	uint16_t programInfoLength;
	uint16_t programNumber;
	PMTItem items[PMT_MAX_ITEMS];
	uint16_t itemsCount;
	bool hasTeletextDescriptor;
} PMTTable;

/**
 * @brief Parse PMT table
 * @param [in] buffer Buffer to parsed
 * @param [out] pmtTable Parsed PMT table
 * @return Error code
 */
uint8_t PMT_Parse(uint8_t* buffer, PMTTable* pmtTable);

/**
 * @brief Print parsed PMT table
 * @param [in] pmtTable PMT table
 * @return Error code
 */
uint8_t PMT_Dump(PMTTable* pmtTable);

#endif
