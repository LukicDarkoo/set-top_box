#include "PMT.h"

uint8_t PMT_Parse(uint8_t* buffer, PMTTable* pmtTable) {
	uint16_t sectionLengthCounter;
	uint16_t i;
	int16_t iDescriptor;

	pmtTable->sectionLength = BitsBufferSlice(buffer, 12, 12);
	pmtTable->programInfoLength = BitsBufferSlice(buffer, 84, 12);
	pmtTable->itemsCount = 0;
	pmtTable->programNumber = BitsBufferSlice(buffer, 24, 16);
	pmtTable->hasTeletextDescriptor = false;

	// Parse streams
	sectionLengthCounter = 12 + pmtTable->programInfoLength;
	while (sectionLengthCounter < pmtTable->sectionLength + 3 - 4) {
		pmtTable->items[pmtTable->itemsCount].streamType = BitsBufferSlice(buffer, sectionLengthCounter * 8, 8);
		pmtTable->items[pmtTable->itemsCount].elementaryPID = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 11, 13);
		pmtTable->items[pmtTable->itemsCount].ESInfoLength = BitsBufferSlice(buffer, sectionLengthCounter * 8 + 28, 12);


		// Parse descriptors
		sectionLengthCounter += 5;
		iDescriptor = pmtTable->items[pmtTable->itemsCount].ESInfoLength;
		while (iDescriptor > 0) {
			// Check if we have teletext descriptor
			if (buffer[sectionLengthCounter] == 0x56) {
				pmtTable->hasTeletextDescriptor = true;
			}

			// Set counters
			sectionLengthCounter += buffer[sectionLengthCounter + 1] + 2;
			iDescriptor -= buffer[sectionLengthCounter + 1] + 2;
		}

		pmtTable->itemsCount++;
	}

	return PMT_SUCCESS;
}

uint8_t PMT_Dump(PMTTable* pmtTable) {
	uint8_t i;

	printf("\n\nPMT Table\n");
	printf("sectionLength = %d\n", pmtTable->sectionLength);
	printf("programInfoLength = %d\n", pmtTable->programInfoLength);
	printf("programNumber = %d\n", pmtTable->programNumber);
	printf("itemsCount = %d\n", pmtTable->itemsCount);

	for (i = 0; i < pmtTable->itemsCount; i++) {
		printf("  streamType = %d\n", pmtTable->items[i].streamType);
		printf("  elementaryPID = %d\n", pmtTable->items[i].elementaryPID);
		printf("  ESInfoLength = %d\n", pmtTable->items[i].ESInfoLength);
		printf("------------------\n");
	}

	return PMT_SUCCESS;
}
