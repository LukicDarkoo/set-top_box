#include "Bits.h"

void BitsByteDump(uint64_t value, uint8_t numberOfBytes) {
    int8_t i;
 
    for (i = numberOfBytes * 8 - 1; i >= 0; i--) {
        printf("%ld", (value >> i) & 1);

		if (i % 8 == 0) {
            printf(" ");
		}
    }
    printf("\n");
}

uint8_t BitsByteSlice(uint8_t byte, uint8_t start, uint8_t length) {
	uint8_t mask = (1 << length) - 1;
    return (byte >> (8 - start - length)) & mask;
}

uint64_t BitsBufferSlice(uint8_t* buffer, uint16_t start, uint8_t length) {
	uint64_t result = 0;
	uint16_t byteIndexStart = start / 8;
	uint8_t bitIndexStart = start % 8;
	int8_t bitsToRead = length;

	while (bitsToRead > 0) {
		uint8_t currentBitsToRead = (bitsToRead < (8 - bitIndexStart)) ? bitsToRead : (8 - bitIndexStart);
		bitsToRead -= currentBitsToRead;
		result |= (BitsByteSlice(buffer[byteIndexStart], bitIndexStart, currentBitsToRead) << bitsToRead);
		byteIndexStart++;
		bitIndexStart = 0;
	}
	
	return result;
}

uint8_t BitsByteSet(uint8_t value, uint8_t position) {
    return (value | (1 << position));
}
 
uint8_t BitsByteClear(uint8_t value, uint8_t position) {
    return ~(1 << position) & value;
}
 
uint8_t BitsByteGet(uint8_t value, uint8_t position) {
    return ((1 << position) & value) >> position;
}
