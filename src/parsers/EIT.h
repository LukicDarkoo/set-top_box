#ifndef _EIT_H_
#define _EIT_H_

#include "Bits.h"
#include <inttypes.h>
#include <string.h>
#include <time.h>

#define EIT_MAX_EVENT_NUMBER 10
#define EIT_MAX_EVENT_DESCRIPTOR_NUMBER 30

#define EIT_SUCCESS 0
#define EIT_ERROR 1
#define EIT_WRONG_TABLE 2

#define EIT_RUNNING_STATUS_RUNNING 4


/**
 * @brief SHort event descriptor of EIT table
 */
typedef struct _EITShortEventDescriptor {
	uint8_t descriptionTag;
	uint8_t descriptionLength;
	uint32_t ISO639LanguageCode;
	uint8_t eventNameLength;
	uint8_t eventName[200];
	uint8_t textLength;
	uint8_t text[200];
} EITShortEventDescriptor;

/**
 * @brief EIT table event. Each EIT table has multiple events
 */
typedef struct _EITEvent {
	uint16_t eventId;
	uint64_t startTime;
	uint32_t duration;
	uint8_t runningStatus;
	uint16_t descriptorsLoopLength;
	EITShortEventDescriptor eitShortEventDescriptor;
} EITEvent;

/**
 * @brief EIT table
 */
typedef struct _EITTable {
	uint16_t sectionLength;
	uint16_t serviceId;
	uint16_t eventCount;
	EITEvent eitEvent[EIT_MAX_EVENT_NUMBER];
} EITTable;

/**
 * @brief Parse EIT table
 * @param [in] buffer Buffer to parsed
 * @param [out] eitTable Pointer to empty EIT table
 * @return Error code
 */
int8_t EIT_Parse(uint8_t* buffer, EITTable* eitTable);

/**
 * @brief Print all parameters of EIT table
 * @param [in] eitTable Pointer to table
 * @return Error code
 */
int8_t EIT_Dump(EITTable* eitTable);

#endif
