#include "HPlayer.h"

/**
 * @brief Volume multiplication factor
 */
static const float VOLUME_MUL_FACTOR = 220 * 1000 * 1000;

/**
 * @brief Stream type can be audio or video
 */
typedef enum t_HPlayer_StreamType {
	HP_AUDIO = 0,//!< HP_AUDIO
	HP_VIDEO = 1 //!< HP_VIDEO
} HPlayer_StreamType;


typedef struct _TdpDtvbTypePair {
	uint8_t dtvb;
	uint8_t tdp;
	HPlayer_StreamType type;
} TdpDtvbTypePair;

/**
 * @brief Pairs of DTV and TDP constants of video and audio types. To add more check page 54 in book
 */
#define TDP_DTVB_TYPE_PAIRS_COUNT 5
static TdpDtvbTypePair TDP_DTVB_TYPE_PAIRS[TDP_DTVB_TYPE_PAIRS_COUNT] = {
	{ 0x01, VIDEO_TYPE_MPEG1, HP_VIDEO },
	{ 0x02, VIDEO_TYPE_MPEG2, HP_VIDEO },
	{ 0x03, AUDIO_TYPE_MPEG_AUDIO, HP_AUDIO },
	{ 0x04, AUDIO_TYPE_MPEG_AUDIO, HP_AUDIO },
	{ 0x0F, AUDIO_TYPE_RAW_AAC, HP_AUDIO }
};

static uint32_t playerHandle;
static uint32_t sourceHandle;
static uint32_t filterHandle;
static uint32_t streamHandle[2];

static bool tunerInitialized;
static bool demuxInitialized;
static bool playerInitialized;
static bool patTableInitialized;
static bool pmtTableInitialized;
static bool streamInitialized[2];
static bool eitTableInitialized;

static PatTable* patTable;
static PMTTable* pmtTable;
static EITTable* eitTable;

/**
 * @brief Play stream
 * @param [in] streamType Type of stream (audio or video)
 * @param [in] pid PID of stream
 * @param [in] dtvbType Type of stream for decoder
 * @return Error code
 */
static int8_t playStream(HPlayer_StreamType streamType, uint16_t pid, int8_t dtvbType);

/**
 * @brief Find correct stream PID in current PMT table
 * @param [in] streamType Required type of stream
 * @param [out] pid Found PID
 * @param [out] dtvbType Found type
 * @return Error code
 */
static int8_t findStreamPID(HPlayer_StreamType streamType, uint16_t* pid, int8_t *dtvbType);

/**
 * @brief Check if dtvb type is audio, video or none
 * @param [in] dtvbType DTVB type
 * @param [in] streamType Required video or audio
 * @return True if type is valid
 */
static bool isTypeValid(int8_t dtvbType, HPlayer_StreamType streamType);

/**
 * @brief Convert DTVB type to DTP type
 * @param [in] tdvb DTVB type
 * @return DTP type
 */
static uint8_t dtvb2tdpType(int8_t dtvb);

/**
 * @brief Function will be called after tuner is locked to frequency
 * @param [in] status Result status
 * @return Error code
 */
static int32_t tunerStatusCallback(t_LockStatus status);

/**
 * @brief Function will be called after table is ready
 * @param [in] buffer Table presented as a buffer
 * @return Error code
 */
static int32_t sectionReceivedCallback(uint8_t *buffer);

/**
 * @brief Filter EIT tables and wait for EIT table which contains current short event name
 * @return Error code
 */
static int8_t filterEIT();

/**
 * @brief Deinitialize a stream
 * @param [in] streamType Audio or video
 * @return Error code
 */
static uint8_t deinitStream(HPlayer_StreamType streamType);


static uint16_t currentStreamPID[2];

static int8_t (*channelChangedCallback)(int32_t channel);
static int8_t (*eventInfoArrivedCallback)();

static pthread_cond_t statusCondition = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t statusMutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t scanCondition = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t scanMutex = PTHREAD_MUTEX_INITIALIZER;

static pthread_cond_t changeChanelCondition = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t changeChanelMutex = PTHREAD_MUTEX_INITIALIZER;


bool HPlayer_HasTeletext() {
	return pmtTable->hasTeletextDescriptor;
}

int8_t HPlayer_GetEventText(char* text) {
	if (eitTableInitialized == false) {
		return HPLAYER_EIT_NOT_INITIALIZED;
	}

	strcpy(text, eitTable->eitEvent[0].eitShortEventDescriptor.eventName);

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_GetEventStartTime(uint64_t* time) {
	if (eitTableInitialized == false) {
		return HPLAYER_EIT_NOT_INITIALIZED;
	}

	*time = eitTable->eitEvent[0].startTime;

	return HPLAYER_SUCCESS;
}

bool HPlayer_HasVideo() {
	return (currentStreamPID[HP_VIDEO] != 0);
}

int8_t HPlayer_SetChangeChannelCallback(int8_t (*callback)(int32_t channel)) {
	channelChangedCallback = callback;

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_SetVolume(uint8_t volume) {
	return Player_Volume_Set(playerHandle, VOLUME_MUL_FACTOR * volume);
}

int8_t HPlayer_SetEventInfoArrivedCallback(int8_t (*callback)()) {
	eventInfoArrivedCallback = callback;

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_GetAudioPID(uint16_t* audioPID) {
	*audioPID = currentStreamPID[HP_AUDIO];

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_GetVideoPID(uint16_t* videoPID) {
	*videoPID = currentStreamPID[HP_VIDEO];

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_GetServiceInfoCount(uint8_t* servicesCount) {
	int32_t i;
	int32_t chanelCounter = 0;
	for (i = 0; i < patTable->serviceInfoCount; i++) {
		if ((patTable)->patServiceInfoArray[i].programNumber != 0) {
			chanelCounter++;
		}
	}

	*servicesCount = chanelCounter;

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_Init() {
	struct timespec lockStatusWaitTime;
	struct timeval now;

	// Set initial values
	playerHandle = 0;
	sourceHandle = 0;
	filterHandle = 0;
	streamHandle[HP_AUDIO] = 0;
	streamHandle[HP_VIDEO] = 0;
	tunerInitialized = false;
	playerInitialized = false;
	patTableInitialized = false;
	demuxInitialized = false;
	pmtTableInitialized = false;
	eitTableInitialized = false;
	streamInitialized[HP_AUDIO] = false;
	streamInitialized[HP_VIDEO] = false;
	eitTable = NULL;
	patTable = NULL;
	pmtTable = NULL;

	// Prepare condition variable timeout
	gettimeofday(&now, NULL);
	lockStatusWaitTime.tv_sec = now.tv_sec + 10;
	lockStatusWaitTime.tv_nsec = 0;

	// Allocate PAT table
	patTable = (PatTable*) malloc(sizeof(PatTable));
	if (patTable == NULL) {
		printf("\n%s : ERROR malloc() fail\n", __FUNCTION__);
	}
	patTableInitialized = true;

	// Initialize tuner device
	if (Tuner_Init()) {
		printf("\n%s : ERROR Tuner_Init() fail\n", __FUNCTION__);
		return HPLAYER_ERROR;
	}
	tunerInitialized = true;

	// Register tuner status callback
	if (Tuner_Register_Status_Callback(tunerStatusCallback)) {
		printf("\n%s : ERROR Tuner_Register_Status_Callback() fail\n",
				__FUNCTION__);
	}

	// Lock to frequency
	if (!Tuner_Lock_To_Frequency(Config_GetInt32("frequency"), Config_GetInt32("bandwidth"), DVB_T)) {
		printf("\n%s: INFO Tuner_Lock_To_Frequency(): %d Hz - success!\n",
				__FUNCTION__, Config_GetInt32("frequency"));
	} else {
		printf("\n%s: ERROR Tuner_Lock_To_Frequency(): %d Hz - fail!\n",
				__FUNCTION__, Config_GetInt32("frequency"));
		HPlayer_Deinit();
		return HPLAYER_ERROR;
	}

	// Wait for tuner to lock
	pthread_mutex_lock(&statusMutex);
	if (ETIMEDOUT
			== pthread_cond_timedwait(&statusCondition, &statusMutex,
					&lockStatusWaitTime)) {
		printf("\n%s:ERROR Lock timeout exceeded!\n", __FUNCTION__);
		HPlayer_Deinit();
		return -1;
	}
	pthread_mutex_unlock(&statusMutex);

	// Initialize player
	if (Player_Init(&(playerHandle))) {
		printf("\n%s : ERROR Player_Init() fail\n", __FUNCTION__);
		HPlayer_Deinit();
		return HPLAYER_ERROR;
	}
	playerInitialized = true;

	// Open source
	if (Player_Source_Open(playerHandle, &(sourceHandle))) {
		printf("\n%s : ERROR Player_Source_Open() fail\n", __FUNCTION__);
		HPlayer_Deinit();
		return HPLAYER_ERROR;
	}

	return HPLAYER_SUCCESS;
}

int8_t filterEIT() {
	struct timespec lockStatusWaitTime;
	struct timeval now;
	int8_t status = HPLAYER_SUCCESS;

	gettimeofday(&now, NULL);
	lockStatusWaitTime.tv_sec = now.tv_sec + 5;
	lockStatusWaitTime.tv_nsec = 0;

	eitTableInitialized = false;

	// Set PST pid and tableID to demultiplexer
	filterHandle = 0;
	if (Demux_Set_Filter(playerHandle, 0x0012, 0x4E,
			&(filterHandle))) {
		printf("\n%s : ERROR Demux_Set_Filter() fail\n", __FUNCTION__);
	}
	demuxInitialized = true;

	// Wait for filter callback
	pthread_mutex_lock(&changeChanelMutex);
	if (ETIMEDOUT
			== pthread_cond_timedwait(&changeChanelCondition,
					&changeChanelMutex, &lockStatusWaitTime)) {
		printf("\n%s:ERROR Lock filter callback exceeded!\n", __FUNCTION__);
		eitTableInitialized = false;
		status = HPLAYER_ERROR;
	}
	pthread_mutex_unlock(&changeChanelMutex);

	// Free filter
	if (Demux_Free_Filter(playerHandle, filterHandle)) {
		printf("\n%s : ERROR Demux_Free_Filter() fail\n", __FUNCTION__);
		eitTableInitialized = false;
		status = HPLAYER_ERROR;
	} else {
		demuxInitialized = false;
	}

	if (status == HPLAYER_SUCCESS) {
		eitTableInitialized = true;
	}

	return status;
}

int8_t HPlayer_SetChanel(int32_t chanel) {
	struct timespec lockStatusWaitTime;
	struct timeval now;
	int16_t pid;
	int32_t i;
	int32_t chanelCounter = 0;
	uint16_t streamPID;
	int8_t dtvbStreamType;


	gettimeofday(&now, NULL);
	lockStatusWaitTime.tv_sec = now.tv_sec + 10;
	lockStatusWaitTime.tv_nsec = 0;

	// Ignore fields where program_number == 0
	for (i = 0; i < patTable->serviceInfoCount; i++) {
		if ((patTable)->patServiceInfoArray[i].programNumber != 0) {
			chanelCounter++;
		}

		if (chanelCounter == chanel) {
			pid = (patTable)->patServiceInfoArray[i].pid;
			break;
		}
	}

	// Set PMT pid and tableID to demultiplexer
	filterHandle = 0;
	if (Demux_Set_Filter(playerHandle, pid, 0x02,
			&(filterHandle))) {
		printf("\n%s : ERROR Demux_Set_Filter() fail\n", __FUNCTION__);
	}
	demuxInitialized = true;

	// Wait for filter callback
	pthread_mutex_lock(&changeChanelMutex);
	if (ETIMEDOUT
			== pthread_cond_timedwait(&changeChanelCondition,
					&changeChanelMutex, &lockStatusWaitTime)) {
		printf("\n%s:ERROR Lock filter callback exceeded!\n", __FUNCTION__);
		HPlayer_Deinit();
		return HPLAYER_ERROR;
	}
	pthread_mutex_unlock(&changeChanelMutex);

	// Free filter
	if (Demux_Free_Filter(playerHandle, filterHandle)) {
		printf("\n%s : ERROR Demux_Free_Filter() fail\n", __FUNCTION__);
	}
	demuxInitialized = false;

	// Play video
	currentStreamPID[HP_VIDEO] = 0;
	if (findStreamPID(HP_VIDEO, &streamPID, &dtvbStreamType) == HPLAYER_SUCCESS) {
		playStream(HP_VIDEO, streamPID, dtvbStreamType);
	} else {
		deinitStream(HP_VIDEO);
	}

	// Play audio
	currentStreamPID[HP_AUDIO] = 0;
	if (findStreamPID(HP_AUDIO, &streamPID, &dtvbStreamType) == HPLAYER_SUCCESS) {
		playStream(HP_AUDIO, streamPID, dtvbStreamType);
	} else {
		deinitStream(HP_AUDIO);
	}

	channelChangedCallback(chanel);

	// Get EIT table
	filterEIT();
	eventInfoArrivedCallback();

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_PreSetChannel(uint16_t videoPID, uint16_t audioPID, uint8_t videoType, uint8_t audioType) {
	if (playStream(HP_VIDEO, videoPID, videoType) != HPLAYER_SUCCESS) {
		return HPLAYER_ERROR;
	}

	if (playStream(HP_AUDIO, audioPID, audioType) != HPLAYER_SUCCESS) {
		return HPLAYER_ERROR;
	}

	return HPLAYER_SUCCESS;
}

uint8_t dtvb2tdpType(int8_t dtvb) {
	uint8_t i;

	for (i = 0; i < TDP_DTVB_TYPE_PAIRS_COUNT; i++) {
		if (dtvb == TDP_DTVB_TYPE_PAIRS[i].dtvb) {
			return TDP_DTVB_TYPE_PAIRS[i].tdp;
		}
	}

	return HPLAYER_ERROR;
}

bool isTypeValid(int8_t dtvbType, HPlayer_StreamType streamType) {
	uint8_t i;

	for (i = 0; i < TDP_DTVB_TYPE_PAIRS_COUNT; i++) {
		if (dtvbType == TDP_DTVB_TYPE_PAIRS[i].dtvb) {
			if (TDP_DTVB_TYPE_PAIRS[i].type == streamType) {
				return true;
			} else {
				return false;
			}
		}
	}

	printf("%s : WARNING dtvbType(0x%x) not found!\n",
					__FUNCTION__, dtvbType);

	return false;
}

int8_t findStreamPID(HPlayer_StreamType streamType, uint16_t* pid, int8_t *dtvbType) {
	size_t i;
	bool pidFound = false;

	// Find PID in PMT
	for (i = 0; i < pmtTable->itemsCount; i++) {
		if (isTypeValid(pmtTable->items[i].streamType, streamType) == true) {
			pidFound = true;
			*dtvbType = pmtTable->items[i].streamType;
			*pid = pmtTable->items[i].elementaryPID;
			break;
		}
	}
	if (pidFound == false) {
		printf("\n%s : ERROR Stream in PMT table is not found\n",
				__FUNCTION__);
		return HPLAYER_ERROR;
	}

	printf("Stream found at PID = %d\n", *pid);

	return HPLAYER_SUCCESS;
}

uint8_t deinitStream(HPlayer_StreamType streamType) {
	// Remove previous
	if (streamInitialized[streamType] == true) {
		Player_Stream_Remove(playerHandle,
			sourceHandle,
			streamHandle[streamType]
		);
		printf("Deinit previously initialized stream\n");
	}
	streamInitialized[streamType] = false;

	return HPLAYER_SUCCESS;
}

int8_t playStream(HPlayer_StreamType streamType, uint16_t pid, int8_t dtvbType) {
	currentStreamPID[streamType] = 0;

	printf("Start stream. AudioVideoType = %d; PID = %d; dtvbType = %d\n", streamType, pid, dtvbType);

	deinitStream(streamType);

	// Play audio
	if (Player_Stream_Create(
			playerHandle,
			sourceHandle,
			pid,
			dtvb2tdpType(dtvbType),
			&(streamHandle[streamType])
		)) {
		printf("\n%s : ERROR Cannot create stream\n", __FUNCTION__);
		return HPLAYER_ERROR;
	}
	printf("New stream started\n");

	currentStreamPID[streamType] = pid;
	streamInitialized[streamType] = true;

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_Scan() {
	struct timespec lockStatusWaitTime;
	struct timeval now;
	gettimeofday(&now, NULL);
	lockStatusWaitTime.tv_sec = now.tv_sec + 10;
	lockStatusWaitTime.tv_nsec = 0;

	// Register section filter callback
	if (Demux_Register_Section_Filter_Callback(sectionReceivedCallback)) {
		printf("\n%s : ERROR Demux_Register_Section_Filter_Callback() fail\n",
				__FUNCTION__);
	}

	// Set PAT pid and tableID to demultiplexer
	if (Demux_Set_Filter(playerHandle, 0x00, 0x00,
			&(filterHandle))) {
		printf("\n%s : ERROR Demux_Set_Filter() fail\n", __FUNCTION__);
	}
	demuxInitialized = true;

	// Wait for filter callback
	pthread_mutex_lock(&scanMutex);
	if (ETIMEDOUT
			== pthread_cond_timedwait(&scanCondition, &scanMutex,
					&lockStatusWaitTime)) {
		printf("\n%s:ERROR Lock filter callback exceeded!\n", __FUNCTION__);
		HPlayer_Deinit();
		return HPLAYER_ERROR;
	}
	pthread_mutex_unlock(&scanMutex);

	// Free filter
	if (Demux_Free_Filter(playerHandle, filterHandle)) {
		printf("\n%s : ERROR Demux_Free_Filter() fail\n", __FUNCTION__);
	}

	return HPLAYER_SUCCESS;
}

int8_t HPlayer_Deinit() {
	uint8_t i;

	if (eitTableInitialized == true) {
		free(eitTable);
	}

	for (i = 0; i < 2; i++) {
		if (streamInitialized[i] == true) {
			Player_Stream_Remove(playerHandle,
					sourceHandle,
					streamHandle[i]
			);
		}
	}


	if (patTableInitialized == true) {
		free(patTable);
	}

	if (pmtTableInitialized == true) {
		free(pmtTable);
	}

	if (demuxInitialized == true) {
		Player_Source_Close(playerHandle, sourceHandle);
	}

	if (playerInitialized == true) {
		Player_Deinit(playerHandle);
	}

	if (tunerInitialized == true) {
		Tuner_Deinit();
	}

	return HPLAYER_SUCCESS;
}

int32_t tunerStatusCallback(t_LockStatus status) {
	if (status == STATUS_LOCKED) {
		pthread_mutex_lock(&statusMutex);
		pthread_cond_signal(&statusCondition);
		pthread_mutex_unlock(&statusMutex);
		printf("\n%s -----TUNER LOCKED-----\n", __FUNCTION__);
	} else {
		printf("\n%s -----TUNER NOT LOCKED-----\n", __FUNCTION__);
	}
	return 0;
}

int32_t sectionReceivedCallback(uint8_t *buffer) {
	uint8_t tableId = *buffer;

	switch (tableId) {
	case 0x00:
		printf("\n%s -----PAT TABLE ARRIVED-----\n", __FUNCTION__);

		if (PAT_Parse(buffer, patTable) == PAT_SUCCESS) {
			PAT_Dump(patTable);

			pthread_mutex_lock(&scanMutex);
			pthread_cond_signal(&scanCondition);
			pthread_mutex_unlock(&scanMutex);
		}
		break;

	case 0x02:
		printf("\n%s -----PMT TABLE ARRIVED-----\n", __FUNCTION__);

		if (pmtTable != NULL) {
			free(pmtTable);
		}
		pmtTable = (PMTTable*) malloc(sizeof(PMTTable));
		PMT_Parse(buffer, pmtTable);
		PMT_Dump(pmtTable);

		pthread_mutex_lock(&changeChanelMutex);
		pthread_cond_signal(&changeChanelCondition);
		pthread_mutex_unlock(&changeChanelMutex);
		break;

	case 0x4E:
		printf("\n%s -----EIT TABLE ARRIVED-----\n", __FUNCTION__);

		// Parse EIT table if program number is required
		if (eitTable != NULL) {
			free(eitTable);
		}
		eitTable = (EITTable*) malloc(sizeof(EITTable));
		EIT_Parse(buffer, eitTable);


		if (eitTable->serviceId != pmtTable->programNumber ||
				eitTable->eitEvent[0].runningStatus != EIT_RUNNING_STATUS_RUNNING) {
			break;
		}
		eitTableInitialized = true;

		EIT_Dump(eitTable);

		// Notify main thread
		pthread_mutex_lock(&changeChanelMutex);
		pthread_cond_signal(&changeChanelCondition);
		pthread_mutex_unlock(&changeChanelMutex);

		break;
	}

	return 0;
}

