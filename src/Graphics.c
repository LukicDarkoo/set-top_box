#include "Graphics.h"

static const char fontPath[] = "/home/galois/fonts/DejaVuSans.ttf";

static VisualState visualState;
static bool refresh;
static pthread_t threadRemote;
static clock_t clockStart;
static IDirectFBSurface *primary = NULL;
static IDirectFB *dfbInterface = NULL;
static int screenWidth = 0;
static int screenHeight = 0;
static DFBSurfaceDescription surfaceDesc;

static bool isRadio = false;
static const uint8_t TRANSPARENT = 150;
static const uint8_t NOT_TRANSPARENT = 0xff;

static IDirectFBFont *H1_FontInterface = NULL;
static DFBFontDescription H1_FontDesc;

static IDirectFBFont *H3_FontInterface = NULL;
static DFBFontDescription H3_FontDesc;

static IDirectFBFont *P_FontInterface = NULL;
static DFBFontDescription P_FontDesc;

static IDirectFBImageProvider* volumeImageProvider[11];
static IDirectFBSurface *volumeImageSurface[11];

/**
 * @brief Convert epoch time integer to string
 * @param [out] parsedTime Formated time
 * @param [in] startTime Epoch time as integer
 * @return Error code
 */
static int8_t parseEpicTime(char* parsedTime, uint64_t startTime);

/**
 * @brief Function which is called in render() to draw channel info dialog
 * @param [in] channelInfoDialog Parameters how to draw
 */
static void drawChannelInfoDialog(ChannelInfoDialog* channelInfoDialog);

/**
 * @brief Function which is called in render() to draw short event dialog
 * @param [in] shortEvent Parameters how to draw
 */
static void drawShortEventDialog(ShortEvent* shortEvent);

/**
 * @brief Function which is called in render() to draw radio screen
 */
static void drawRadioScreen();

/**
 * @brief Function which is called in render() to draw no channel screen
 */
static void drawNoChannelScreen();

/**
 * @brief Function which is called in render() to draw volume dialog
 * @param [in] volume Parameters how to draw
 */
static void drawVolume(Volume* volume);

/**
 * @brief Run in separated thread and redraw graphics elements every 16.6ms
 */
static void* render();

static Timer timerChannelInfoDialog;
static Timer timerEventDialog;
static Timer timerVolume;

static void hideChannelInfoDialog();
static void hideEventDialog();
static void hideVolume();

int8_t Graphics_Init(int32_t argc, char** argv) {
	size_t i;

	// Init visual state defaults
	refresh = true;

	DFBCHECK(DirectFBInit(&argc, &argv));

    // Fetch the DirectFB interface
	DFBCHECK(DirectFBCreate(&dfbInterface));

    // Tell the DirectFB to take the full screen for this application
	DFBCHECK(dfbInterface->SetCooperativeLevel(dfbInterface, DFSCL_FULLSCREEN));

    // Create primary surface with double buffering enabled
	surfaceDesc.flags = DSDESC_CAPS;
	surfaceDesc.caps = DSCAPS_PRIMARY | DSCAPS_FLIPPING;
	DFBCHECK (dfbInterface->CreateSurface(dfbInterface, &surfaceDesc, &primary));

    // Fetch the screen size
    DFBCHECK (primary->GetSize(primary, &screenWidth, &screenHeight));


    // Initialize fonts
    H1_FontDesc.flags = DFDESC_HEIGHT;
    H1_FontDesc.height = 120;
    DFBCHECK(dfbInterface->CreateFont(dfbInterface, fontPath, &H1_FontDesc, &H1_FontInterface));

    H3_FontDesc.flags = DFDESC_HEIGHT;
	H3_FontDesc.height = 80;
	DFBCHECK(dfbInterface->CreateFont(dfbInterface, fontPath, &H3_FontDesc, &H3_FontInterface));

    P_FontDesc.flags = DFDESC_HEIGHT;
    P_FontDesc.height = 32;
    DFBCHECK(dfbInterface->CreateFont(dfbInterface, fontPath, &P_FontDesc, &P_FontInterface));


    // Init images
    for (i = 0; i <= 10; i++) {
    	char imagePath[30];

    	sprintf(imagePath, "assets/volume_%02d.png", i);
    	printf("imagePath = %s\n", imagePath);
		DFBCHECK(dfbInterface->CreateImageProvider(dfbInterface, imagePath, &(volumeImageProvider[i])));
		DFBCHECK(volumeImageProvider[i]->GetSurfaceDescription(volumeImageProvider[i], &surfaceDesc));
		DFBCHECK(dfbInterface->CreateSurface(dfbInterface, &surfaceDesc, &(volumeImageSurface[i])));
		DFBCHECK(volumeImageProvider[i]->RenderTo(volumeImageProvider[i], volumeImageSurface[i], NULL));
		volumeImageProvider[i]->Release(volumeImageProvider[i]);
    }

    // Init timers
    Timer_Init(&timerChannelInfoDialog, hideChannelInfoDialog);
    Timer_Init(&timerEventDialog, hideEventDialog);
    Timer_Init(&timerVolume, hideVolume);


	pthread_create(&threadRemote, NULL, &render, NULL);
	return GRAPHICS_SUCCESS;
}

void hideChannelInfoDialog() {
	visualState.channelInfoDialog.show = false;
}

void hideEventDialog() {
	visualState.shortEvent.show = false;
}

void hideVolume() {
	visualState.volume.show = false;
}

int8_t Graphics_Deinit() {
	Timer_Deinit(&timerChannelInfoDialog);
	Timer_Deinit(&timerEventDialog);
	Timer_Deinit(&timerVolume);

	refresh = false;
	pthread_join(threadRemote, NULL);

	return GRAPHICS_SUCCESS;
}


int8_t Graphics_SetRadioScreen(bool show) {
	visualState.radioScreen = show;

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_SetTeletext(bool exists) {
	visualState.channelInfoDialog.teletextAvailable = exists;

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_SetVolume(uint8_t volume) {
	visualState.volume.imageIndex = volume;
	return GRAPHICS_SUCCESS;
}

int8_t Graphics_ShowVolume(uint32_t msDuration) {
	visualState.volume.show = true;
	Timer_SetDelay(&timerVolume, msDuration);

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_ShowNoChannel(bool show) {
	visualState.noChannel = show;
	return GRAPHICS_SUCCESS;
}

int8_t Graphics_SetChannelNumber(uint8_t channel) {
	visualState.channelInfoDialog.number = channel;

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_ShowShortEventDialog(uint32_t msDuration) {
	visualState.shortEvent.show = true;
	Timer_SetDelay(&timerEventDialog, msDuration);

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_ShowChannelInfoDialog(uint32_t msDuration) {
	visualState.channelInfoDialog.show = true;
	Timer_SetDelay(&timerChannelInfoDialog, msDuration);

	return GRAPHICS_SUCCESS;
}

int8_t Graphics_SetPIDs(uint16_t audioPID, uint16_t videoPID) {
	visualState.channelInfoDialog.audioPID = audioPID;
	visualState.channelInfoDialog.videoPID = videoPID;

	return GRAPHICS_SUCCESS;
}

void drawShortEventDialog(ShortEvent* shortEvent) {
	DFBCHECK(primary->SetColor(primary, 0, 0, 0, isRadio ? NOT_TRANSPARENT : TRANSPARENT));
	DFBCHECK(primary->FillRectangle(primary, 0, screenHeight - 300, screenWidth, 300));

	// Show event text
	DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, H3_FontInterface));
	DFBCHECK(primary->DrawString(primary, shortEvent->text, -1, 90, screenHeight - 150, DSTF_LEFT));

	// Show event start time
	DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, P_FontInterface));
	DFBCHECK(primary->DrawString(primary, shortEvent->startTime, -1, 90, screenHeight - 100, DSTF_LEFT));
}

int8_t Graphics_SetShortEvent(char* text, uint64_t startTime) {
	strcpy(visualState.shortEvent.text, text);

	parseEpicTime(visualState.shortEvent.startTime, startTime);

	return GRAPHICS_SUCCESS;
}


static void drawNoChannelScreen() {
	DFBCHECK(primary->SetColor(primary, 0x00, 0x00, 0x00, NOT_TRANSPARENT));
	DFBCHECK(primary->FillRectangle(primary, 0, 0, screenWidth, screenHeight));

	// Show radio screen
	DFBCHECK(primary->SetColor(primary, 192, 57, 43, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, H1_FontInterface));
	DFBCHECK(primary->DrawString(primary, "No Channel :(", -1, screenWidth / 2 - 400, screenHeight / 2, DSTF_LEFT));
}

static void drawVolume(Volume* volume) {
	DFBCHECK(primary->Blit(primary, volumeImageSurface[volume->imageIndex], NULL, screenWidth - 250, 50));
}

void drawChannelInfoDialog(ChannelInfoDialog* channelInfoDialog) {
	char channelNumber[3];
	char audioPID[6];
	char videoPID[6];

	audioPID[0] = 'A';
	videoPID[0] = 'V';
	sprintf(channelNumber, "%02d", channelInfoDialog->number);
	sprintf((audioPID + 1), "%04d", channelInfoDialog->audioPID);
	sprintf((videoPID + 1), "%04d", channelInfoDialog->videoPID);

	// Draw rectangle
	DFBCHECK(primary->SetColor(primary, 0, 0, 0, isRadio ? NOT_TRANSPARENT : TRANSPARENT));
	DFBCHECK(primary->FillRectangle(primary, 50, 50, 400, 130));

	// Draw channel number
	DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, H1_FontInterface));
	DFBCHECK(primary->DrawString(primary, channelNumber, -1, 75, 160, DSTF_LEFT));

	// Draw teletext
	DFBCHECK(primary->SetColor(primary, 192, 57, 43, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, P_FontInterface));
	DFBCHECK(primary->DrawString(primary, channelInfoDialog->teletextAvailable ? "TELETEXT" : "", -1, 280, 90, DSTF_LEFT));

	// Draw Audio PID
	if (channelInfoDialog->audioPID != 0) {
		DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
		DFBCHECK(primary->SetFont(primary, P_FontInterface));
		DFBCHECK(primary->DrawString(primary, audioPID, -1, 280, 125, DSTF_LEFT));
	}

	// Draw Video PID
	if (channelInfoDialog->videoPID != 0) {
		isRadio = false;
		DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
		DFBCHECK(primary->SetFont(primary, P_FontInterface));
		DFBCHECK(primary->DrawString(primary, videoPID, -1, 280, 160, DSTF_LEFT));
	} else {
		isRadio = true;
	}
}

void drawRadioScreen() {
	DFBCHECK(primary->SetColor(primary, 0x00, 0x00, 0x00, NOT_TRANSPARENT));
	DFBCHECK(primary->FillRectangle(primary, 0, 0, screenWidth, screenHeight));

	// Show radio screen
	DFBCHECK(primary->SetColor(primary, 46, 204, 113, NOT_TRANSPARENT));
	DFBCHECK(primary->SetFont(primary, H1_FontInterface));
	DFBCHECK(primary->DrawString(primary, "Radio", -1, screenWidth / 2 - 200, screenHeight / 2, DSTF_LEFT));
}

int8_t parseEpicTime(char* parsedTime, uint64_t startTime) {
	uint8_t K;
	uint16_t Y;
	uint8_t M;

	// Calculate time
	uint8_t mm_prim = ((startTime & 0x000000FF00) >> 8);
	uint8_t ss_prim = (startTime & 0x00000000FF);
	uint8_t hh_prim = ((startTime & 0x0000FF0000) >> 16);
	uint8_t mm = (mm_prim / 16) * 10 + mm_prim % 16;
	uint8_t ss = (ss_prim / 16) * 10 + ss_prim % 16;
	uint8_t hh = (hh_prim / 16) * 10 + hh_prim % 16;

	// Calculate date
	uint16_t MJD = (uint16_t)((startTime & 0xFFFF000000) >> 24);
	uint8_t Y_prim = (((float)MJD - 15078.2) / 365.25);
	uint8_t M_prim = (((float)MJD - 14956.1 - (float)((int)(Y_prim * 365.25))) / 30.6001);
	uint8_t D = ((float)MJD - 14956 - (int)(Y_prim * 365.25) - (int)((float)M_prim * 30.6001));


	if (M_prim == 14 || M_prim == 15) {
		K = 1;
	} else {
		K = 0;
	}
	Y = Y_prim + K + 1900;
	M = M_prim - 1 - K * 12;

	sprintf(parsedTime, "%d. %d. %d. %02d:%02d:%02d", D, M, Y, hh, mm, ss);

	return GRAPHICS_SUCCESS;
}


void* render() {
	while (refresh == true) {
		 clockStart = clock();

		 // Clear graphics
		 DFBCHECK(primary->SetColor(primary, 0x00, 0x00, 0x00, 0x00));
		 DFBCHECK(primary->FillRectangle(primary, 0, 0, screenWidth, screenHeight));

		 // Draw radio screen
		 if (visualState.radioScreen == true) {
			 drawRadioScreen();
		 }

		 // Draw no channel screen
		 if (visualState.noChannel == true) {
			 drawNoChannelScreen();
		 }

		// Draw Channel Info Dialog
		if (visualState.channelInfoDialog.show == true) {
			drawChannelInfoDialog(&visualState.channelInfoDialog);
		}

		// Draw Short Info Dialog
		if (visualState.shortEvent.show == true && strcmp(visualState.shortEvent.text, "") != 0) {
			drawShortEventDialog(&visualState.shortEvent);
		}

		// Draw Volume
		if (visualState.volume.show == true) {
			drawVolume(&visualState.volume);
		}

		// Switch between the displayed and the work buffer (update the display)
		DFBCHECK(primary->Flip(primary, NULL, 0));

		float processTimeMS = ((float)(clock() - clockStart) / 1000000.0F ) * 1000;
		if (processTimeMS > GRAPHICS_REFRESH_PERIOD) {
			usleep((GRAPHICS_REFRESH_PERIOD - processTimeMS) * 1000);
		}
	}

	primary->Release(primary);
	dfbInterface->Release(dfbInterface);

	return (void*)(GRAPHICS_SUCCESS);
}
