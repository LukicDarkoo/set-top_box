var indexSectionsWithContent =
{
  0: "_abcdeghilmnprstv",
  1: "_",
  2: "abceghprt",
  3: "bceghmprt",
  4: "acdehilnprstv",
  5: "cehpstv",
  6: "t",
  7: "h",
  8: "cdeghnprt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

