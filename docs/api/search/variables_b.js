var searchData=
[
  ['tableid',['tableId',['../struct__PatHeader.html#a5cb0798a5edc1a15b6f5ea77546b48f4',1,'_PatHeader']]],
  ['tdp',['tdp',['../struct__TdpDtvbTypePair.html#a6823a36063539ef3a6450b2c018a4af4',1,'_TdpDtvbTypePair']]],
  ['teletextavailable',['teletextAvailable',['../struct__ChannelInfoDialog.html#a8b19398e74f773947d6687c4027d52ef',1,'_ChannelInfoDialog']]],
  ['text',['text',['../struct__ShortEvent.html#a6dae1f526202a98d7fb62f662e641df3',1,'_ShortEvent::text()'],['../struct__EITShortEventDescriptor.html#a65d204324253217eacd3158041b70da4',1,'_EITShortEventDescriptor::text()']]],
  ['textlength',['textLength',['../struct__EITShortEventDescriptor.html#a96f130ca98794bdd8335ee5dd66bd649',1,'_EITShortEventDescriptor']]],
  ['timerid',['timerId',['../struct__Timer.html#afe13c710d86413c91cebd59bbc5d50a9',1,'_Timer']]],
  ['timerspec',['timerSpec',['../struct__Timer.html#ab201cd99d91d0990167e76970d2d7612',1,'_Timer']]],
  ['timerspecold',['timerSpecOld',['../struct__Timer.html#abffcbde7222f679556c2f573f6f9bd2a',1,'_Timer']]],
  ['transportstreamid',['transportStreamId',['../struct__PatHeader.html#a02fb14368fc5e7e8be398267447d97ea',1,'_PatHeader']]],
  ['type',['type',['../struct__TdpDtvbTypePair.html#a6ddb57e516321ccc63246e17758f350f',1,'_TdpDtvbTypePair']]]
];
