var searchData=
[
  ['timer_5fdeinit',['Timer_Deinit',['../Timer_8c.html#a01f5313753b11c5750771c2ecdceae29',1,'Timer_Deinit(Timer *timer):&#160;Timer.c'],['../Timer_8h.html#a01f5313753b11c5750771c2ecdceae29',1,'Timer_Deinit(Timer *timer):&#160;Timer.c']]],
  ['timer_5finit',['Timer_Init',['../Timer_8c.html#a773c592f8966df7225b190ff6824cf84',1,'Timer_Init(Timer *timer, void(*callback)()):&#160;Timer.c'],['../Timer_8h.html#a773c592f8966df7225b190ff6824cf84',1,'Timer_Init(Timer *timer, void(*callback)()):&#160;Timer.c']]],
  ['timer_5fsetdelay',['Timer_SetDelay',['../Timer_8c.html#a7cf355ae5aed7ffd2546424782d62e44',1,'Timer_SetDelay(Timer *timer, uint16_t ms):&#160;Timer.c'],['../Timer_8h.html#a7cf355ae5aed7ffd2546424782d62e44',1,'Timer_SetDelay(Timer *timer, uint16_t ms):&#160;Timer.c']]]
];
